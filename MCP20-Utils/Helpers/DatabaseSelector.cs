﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MCP20_Utils
{
    public static class DatabaseSelector
    {
        public static string GetCustomerDBConnectionString(int customerId)
        {
            SqlConnection foundationDBConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["FoundationDB"].ConnectionString);
            DataSet customerList = new DataSet();
            DataRow customerInfo;

            foundationDBConnection.Open();

            SqlCommand readCustomerList = new SqlCommand("SELECT * FROM CUSTOMERS WHERE CUSTOMERID=" + customerId.ToString(), foundationDBConnection);
            SqlDataAdapter sqlAdapter = new SqlDataAdapter(readCustomerList);

            sqlAdapter.Fill(customerList);
            customerInfo = customerList.Tables[0].Rows[0];

            try
            {
                // Build the connection string from the provided datasource and database
                var connString = ConfigurationManager.ConnectionStrings["CustomerDB"].ConnectionString;
                connString = connString.Replace("{data_source}", customerInfo["DbServer"].ToString());
                connString = connString.Replace("{customer_db}", customerInfo["DbName"].ToString());

                return connString;
            }

            catch (ArgumentException ex)
            {
                return "Invalid data found! " + ex.Message;
            }

            finally
            {
                foundationDBConnection.Close();
            }
        }

        public static string GetCustomerDBConnectionString(DataRow customerTableRow)
        {
            try
            {
                // Build the connection string from the provided datasource and database
                var connString = ConfigurationManager.ConnectionStrings["CustomerDB"].ConnectionString;
                connString = connString.Replace("{data_source}", customerTableRow["DbServer"].ToString());
                connString = connString.Replace("{customer_db}", customerTableRow["DbName"].ToString());

                return connString;
            }

            catch (ArgumentException ex)
            {
                return "Invalid datarow supplied! " + ex.Message;
            }

        }

    }
}