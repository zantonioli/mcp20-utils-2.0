﻿<%@ Page Title="Edit Roster Fields" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditRosterFields.aspx.cs" Inherits="MCP20_Utils.EditRosterFields" %>
<%@Import Namespace="System.Data"%>


<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <link rel="stylesheet" type="text/css" href="Content/bootstrap.css">
    <script type="text/javascript">
         function confirmDelete() {
                return confirm("Are you sure?");
         }

        function enableTextBox() {
            $('#<%=displayNameTextBox.ClientID%>').removeProp('disabled');
            $('#<%=displayNameTextBox.ClientID%>').removeClass('aspNetDisabled');

        }

        $(document).ready(function () {
            $('input[id*=tb_]').on('input propertychange paste', function (event) {
                $('#<%=btnModify.ClientID %>').removeProp('disabled');
                $('#<%=btnModify.ClientID %>').removeClass('aspNetDisabled');
                $('#<%=btnModifyDup.ClientID %>').removeProp('disabled');
                $('#<%=btnModifyDup.ClientID %>').removeClass('aspNetDisabled');
                $(event.target).addClass("tb_changed");
            });

            $('#<%=rosterFieldsGridView.ClientID%> input[type=checkbox]').on('change', function (event) {
                $('#<%=btnModify.ClientID %>').removeProp('disabled');
                $('#<%=btnModify.ClientID %>').removeClass('aspNetDisabled');
                $('#<%=btnModifyDup.ClientID %>').removeProp('disabled');
                $('#<%=btnModifyDup.ClientID %>').removeClass('aspNetDisabled');
                $(event.target).parent().parent().toggleClass("tb_changed");
            });

            $('#<%=displayNameTextBox.ClientID%>').on('input propertychange paste', function (event) {
                var temp = $('#<%=fieldNameListBox.ClientID%>');
                var temp2 = $('#<%=fieldNameListBox.ClientID%>').text();
                var temp2 = $('#<%=fieldNameListBox.ClientID%>').val();
                if ($('#<%=displayNameTextBox.ClientID%>').val() != "" && $('#<%=fieldNameListBox.ClientID%>').text != "Select..." && $('#<%=fieldNameListBox.ClientID%>').val() != "") {
                    $('#<%=btn_addRosterField.ClientID%>').removeProp('disabled');
                    $('#<%=btn_addRosterField.ClientID%>').removeClass('aspNetDisabled');
                }
                else {
                    $('#<%=btn_addRosterField.ClientID%>').prop('disabled','disabled');
                    $('#<%=btn_addRosterField.ClientID%>').addClass('aspNetDisabled');
                }

            })
           
        });
    </script>

    <style type="text/css">

        .tb_changed {
            background-color: khaki
        }

        

    </style>

    <h1>Add/Remove/Edit Roster Fields</h1>
    <h3>Customer List</h3>
    <div class="container">
        <div class="row">
            <div class="col-lg-4" id="customerListContainer">
                <div class="row">
                    <asp:ListBox runat="server" ID="customerListBox" DataTextField="DisplayName" Rows="10" DataValueField="CustomerId" OnSelectedIndexChanged="CustomerListBox_SelectedIndexChanged" AutoPostBack="true" ></asp:ListBox>
                </div>
                <div class="row">
                    <asp:Label runat="server" id="add_roster_span" Visible="false">ADD ROSTER FIELD</asp:Label>
                </div>
                <asp:Panel runat="server" ID="addRosterFieldPanel" Visible="false">
                    <asp:Label runat="server">Database Table</asp:Label>
                    <asp:DropDownList runat="server" ID="tableListBox" AutoPostBack="true" DataTextField="TableName" DataValueField="DatabaseTableID" OnSelectedIndexChanged="TableListBox_SelectedIndexChanged"></asp:DropDownList>
                    <br />
                    <asp:Label runat="server">Field Name</asp:Label>
                    <asp:DropDownList runat="server" ID="fieldNameListBox" DataTextField="COLUMN_NAME" DataValueField="COLUMN_NAME"></asp:DropDownList>
                    <br />
                    <asp:Label runat="server">Display Name</asp:Label>
                    <asp:TextBox runat="server" ID="displayNameTextBox"></asp:TextBox>
                    <br />
                    <asp:Button runat="server" Enabled="false" ID="btn_addRosterField" Text="Add Roster Field" OnClientClick="" OnClick="Btn_addRosterField_Click" />
                </asp:Panel>
                
            </div>
            <span>NOTE: If delete button is disabled, the roster field is currently mapped.</span>

            <div class="col-lg-8" id="rosterFieldsContainer">
                <asp:Button runat="server" ID="btnModifyDup" Enabled="false" Text="Save Changes" Visible="false"  OnClick="btnModify_Click" />
                <asp:GridView runat="server" ID="rosterFieldsGridView" AutoGenerateColumns="false" AlternatingItemStyle-BackColor="#cffcfc" DataKeyNames="FieldID" >
                    <Columns>
                        <asp:TemplateField HeaderText="Delete?">
                            <ItemTemplate>
                                <asp:Button runat="server" ID="btn_Delete" Text='Delete' Enabled='<%#Convert.ToBoolean(((DataRowView)Container.DataItem)["CANDELETE"])%>' OnClientClick="return confirmDelete();" OnClick="Btn_Delete_Click"/>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="FieldID" HeaderText="Field ID"></asp:BoundField>
                        <asp:BoundField DataField="TableName" HeaderText="Table Name/Field Category"></asp:BoundField>                        
                        <asp:TemplateField HeaderText="Display Name">
                            <ItemTemplate>
                                <asp:TextBox runat="server" ID="tb_DisplayName" Text='<%#DataBinder.Eval(Container.DataItem, "DisplayName")%>' Enabled='<%#Convert.ToBoolean(((DataRowView)Container.DataItem)["USEDISPLAYNAME"])%>' ></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Field Name">
                            <ItemTemplate>
                                <asp:TextBox runat="server" ID="tb_FieldName" Text='<%#DataBinder.Eval(Container.DataItem, "FieldName")%>' Enabled='<%#Convert.ToBoolean(((DataRowView)Container.DataItem)["USEFIELDNAME"])%>' ></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        <div class="row">
            <asp:Button runat="server" ID="btnModify" Enabled="false" Visible="false" Text="Save Changes" OnClick="btnModify_Click" />
        </div>
    </div>


</asp:Content>

