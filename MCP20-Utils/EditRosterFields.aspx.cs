﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace MCP20_Utils
{
    public partial class EditRosterFields : System.Web.UI.Page
    {
        DataView rosterFieldsView;

        SqlConnection foundationDBConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["FoundationDB"].ConnectionString);
        SqlConnection customerDBConnection = new SqlConnection();

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                DataSet customerList = new DataSet();

                foundationDBConnection.Open();

                SqlCommand readCustomerList = new SqlCommand("SELECT * FROM CUSTOMERS WHERE SHOWINTRIAGE=1 ORDER BY DISPLAYNAME ASC", foundationDBConnection);
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(readCustomerList);
                sqlAdapter.Fill(customerList);

                if (customerList.Tables[0].Rows.Count == 0)
                {
                    //bad
                    //TODO: Something more meaningful
                    return;
                }
                else
                {
                    customerListBox.DataSource = customerList.Tables[0];
                    customerListBox.DataBind();
                }
                
                foundationDBConnection.Close();
            }
            else
            {
                ViewState["customerDBConnectionString"] = DatabaseSelector.GetCustomerDBConnectionString(Int32.Parse(customerListBox.SelectedValue));
                customerDBConnection.ConnectionString = ViewState["customerDBConnectionString"].ToString();
            }
            
        }

        protected void CustomerListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            rosterFieldsView = GetRosterFieldsData().DefaultView;
            rosterFieldsGridView.DataSource = rosterFieldsView;
            
            rosterFieldsGridView.DataBind();

            btnModifyDup.Visible = true;
            btnModify.Visible = true;
            addRosterFieldPanel.Visible = true;
            tableListBox.DataSource = GetDatabaseTablesData();
            tableListBox.DataBind();

            tableListBox.Items.Insert(0, new ListItem("Select...", string.Empty));
        }

        private DataTable GetRosterFieldsData()
        {
            DataSet rosterFieldsList = new DataSet();

            customerDBConnection.Open();

            SqlCommand readRosterFieldsList = new SqlCommand("SELECT RF.FIELDID, RF.ALLOWDUPLICATES, RF.FIELDNAME, RF.DISPLAYNAME, DTBS.TABLENAME,"+
                "CASE WHEN DTBS.TABLENAME IN('CourseInstructors', 'CourseStudents') THEN 1 ELSE 0 END AS USEFIELDNAME,"+
                "CASE WHEN DTBS.TABLENAME NOT IN('CourseInstructors', 'CourseStudents') THEN 1 ELSE 0 END AS USEDISPLAYNAME, "+
                "CASE WHEN FM.FIELDMAPPINGID IS NULL THEN 1 ELSE 0 END AS CANDELETE "+
                "FROM ROSTERFIELDS RF INNER JOIN DATABASETABLES DTBS ON RF.DATABASETABLEID = DTBS.DATABASETABLEID "+
                "LEFT JOIN FIELDMAPPINGS FM ON FM.ROSTERFIELDID = RF.FIELDID ORDER BY RF.FIELDID ASC", customerDBConnection);
            SqlDataAdapter sqlAdapter = new SqlDataAdapter(readRosterFieldsList);
            sqlAdapter.Fill(rosterFieldsList);
            customerDBConnection.Close();

            return rosterFieldsList.Tables[0];
        }

        private DataTable GetDatabaseTablesData()
        {
            DataSet dbTablesList = new DataSet();

            customerDBConnection.Open();

            SqlCommand readDBTablesList = new SqlCommand("SELECT * FROM DATABASETABLES", customerDBConnection);
            SqlDataAdapter sqlAdapter = new SqlDataAdapter(readDBTablesList);
            sqlAdapter.Fill(dbTablesList);
            customerDBConnection.Close();

            return dbTablesList.Tables[0];
        }

        private DataTable BuildRosterFieldChanges()
        {
            DataTable freshData = GetRosterFieldsData();

            //Trim out unneeded columns
            freshData.Columns.Remove("USEDISPLAYNAME");
            freshData.Columns.Remove("USEFIELDNAME");
            freshData.Columns.Remove("CANDELETE");
            freshData.Columns.Remove("TABLENAME");
            freshData.Columns.Remove("ALLOWDUPLICATES");

            DataTable gvTable = freshData.Clone();

            foreach (GridViewRow gvr in rosterFieldsGridView.Rows)
            {
                Int32 fieldid = Int32.Parse(gvr.Cells[1].Text);
                string fieldname = ((TextBox)gvr.Cells[3].Controls[1]).Text.Trim();
                string displayname = ((TextBox)gvr.Cells[4].Controls[1]).Text.Trim();

                gvTable.Rows.Add(fieldid, displayname, fieldname);
            }

            DataTable returnTable = freshData.Clone();

            //Get differences between current and prior data
            var differentRows = gvTable.AsEnumerable().Where(r => freshData.AsEnumerable().Any(r2 => r["FIELDID"].ToString().Trim() == r2["FIELDID"].ToString().Trim() &&
                (r["FIELDNAME"].ToString().Trim() != r2["FIELDNAME"].ToString().Trim() ||
                r["DISPLAYNAME"].ToString().Trim() != r2["DISPLAYNAME"].ToString().Trim())));

            if (differentRows.Any())
            {
                returnTable = differentRows.CopyToDataTable();
            }

            return returnTable;
        }

        protected void btnModify_Click(object sender, EventArgs e)
        {
            DataTable changes = BuildRosterFieldChanges();

            customerDBConnection.Open();
            foreach (DataRow r in changes.Rows)
            {
                SqlCommand updateRosterFieldsTable = new SqlCommand("UPDATE ROSTERFIELDS SET " +
                "FIELDNAME = '" + r["FIELDNAME"] + "', DISPLAYNAME = '" + r["DISPLAYNAME"] + "' WHERE FIELDID = " + r["FIELDID"]
                , customerDBConnection);
                updateRosterFieldsTable.ExecuteNonQuery();

            }
            customerDBConnection.Close();
        }

        protected void Btn_Delete_Click(object sender, EventArgs e)
        {
            DataSet fieldMappingsList = new DataSet();
            Button deleteButton = (Button)sender;
            GridViewRow gvr = (GridViewRow)deleteButton.NamingContainer;

            var fieldID = rosterFieldsGridView.DataKeys[gvr.RowIndex].Value;

            customerDBConnection.Open();

            SqlCommand readFieldMappingsList = new SqlCommand("SELECT * FROM FIELDMAPPINGS WHERE ROSTERFIELDID = " + fieldID, customerDBConnection);
            SqlDataAdapter sqlAdapter = new SqlDataAdapter(readFieldMappingsList);
            sqlAdapter.Fill(fieldMappingsList);

            if (fieldMappingsList.Tables[0].Rows.Count > 0)
            {
                return;
            }

            SqlCommand deleteRosterField = new SqlCommand("DELETE FROM ROSTERFIELDS WHERE FIELDID = " + fieldID, customerDBConnection);
            deleteRosterField.ExecuteNonQuery();

            customerDBConnection.Close();

            rosterFieldsView = GetRosterFieldsData().DefaultView;
            rosterFieldsGridView.DataSource = rosterFieldsView;

            rosterFieldsGridView.DataBind();
        }

        protected void Btn_addRosterField_Click(object sender, EventArgs e)
        {
            string displayName = displayNameTextBox.Text;
            string fieldName = fieldNameListBox.SelectedItem.Value;
            string tableName = tableListBox.SelectedItem.Text;
            string tableId = tableListBox.SelectedItem.Value;
            

            int max = -1;

            //find current max FieldID
            foreach (DataKey x in rosterFieldsGridView.DataKeys)
            {
                if (Int32.Parse(x["FieldID"].ToString()) > max)
                {
                    max = Int32.Parse(x["FieldID"].ToString());
                }
            }

            customerDBConnection.Open();

            

            SqlCommand addRosterField = new SqlCommand("INSERT INTO ROSTERFIELDS (FieldID,FieldName,DisplayName,ParentRosterObjectID,DatabaseTableID,IsActive) " + 
                "VALUES(" + (max+1).ToString() + ",'" + fieldName + "','" + displayName + "'," + tableId + "," + tableId + ",'True')", customerDBConnection);
            addRosterField.ExecuteNonQuery();

            customerDBConnection.Close();

            //Reset view
            rosterFieldsView = GetRosterFieldsData().DefaultView;
            rosterFieldsGridView.DataSource = rosterFieldsView;
            rosterFieldsGridView.DataBind();
        }

        protected void TableListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet unusedColumnsList = new DataSet();
            //get field info
            customerDBConnection.Open();



           SqlCommand getUnusedColumns = new SqlCommand(
            "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS ISC " +
            "LEFT JOIN ROSTERFIELDS RF ON RF.FIELDNAME = ISC.COLUMN_NAME " +
            "LEFT JOIN DATABASETABLES DT ON DT.DATABASETABLEID = RF.PARENTROSTEROBJECTID " +
            "WHERE ISC.TABLE_NAME = '" + tableListBox.SelectedItem.Text + "' AND ISC.DATA_TYPE IN('nvarchar', 'varchar') AND RF.FIELDID IS NULL", customerDBConnection);
            SqlDataAdapter sqlAdapter = new SqlDataAdapter(getUnusedColumns);
            sqlAdapter.Fill(unusedColumnsList);

            customerDBConnection.Close();

            fieldNameListBox.DataSource = unusedColumnsList.Tables[0];
            fieldNameListBox.DataBind();

            fieldNameListBox.Items.Insert(0, new ListItem("Select...", string.Empty));
        }
    }

}